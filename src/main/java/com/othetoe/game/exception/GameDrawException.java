package com.othetoe.game.exception;

public class GameDrawException extends Exception {
    private String message;

    public GameDrawException(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
