package com.othetoe.game.exception;

public class GameEndedException extends Exception {
  private String message;

  public GameEndedException(String message) {
      this.message = message;
  }

  public String getMessage() {
    return message;
  }
}
