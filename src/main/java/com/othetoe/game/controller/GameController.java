package com.othetoe.game.controller;

import java.util.HashMap;

import javax.validation.Valid;

import com.othetoe.game.exception.GameDrawException;
import com.othetoe.game.exception.InvalidGameException;
import com.othetoe.game.exception.InvalidMoveException;
import com.othetoe.game.exception.InvalidParamException;
import com.othetoe.game.exception.NotFoundException;
import com.othetoe.game.helper.ResponseData;
import com.othetoe.game.model.dto.GameData;
import com.othetoe.game.model.dto.GamePlay;
import com.othetoe.game.model.entity.Game;
import com.othetoe.game.model.entity.GameStatus;
import com.othetoe.game.model.entity.User;
import com.othetoe.game.model.repository.GameRepo;
import com.othetoe.game.model.service.GameService;
import com.othetoe.game.model.service.UserService;
import com.othetoe.game.storage.GameStorage;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping("/api/game")
public class GameController {

  @Autowired
  GameService gameService;

  @Autowired
  ModelMapper modelMapper;

  @Autowired
  UserService userService;

  @PostMapping()
  public ResponseEntity<Object> create(@RequestBody @Valid GameData gameData, Errors errors) {
    ResponseData<Object> responseData = new ResponseData<>();
    if (errors.hasErrors()) {
      for (FieldError error : errors.getFieldErrors()) {
        responseData.failed(error.getField(), error.getDefaultMessage());
      }
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
    }

    // get authenticated user
    String username = SecurityContextHolder.getContext().getAuthentication().getName();
    User user = userService.getByUsername(username);
    user.setTurns(1);

    Game game = modelMapper.map(gameData, Game.class);
    game.setPlayer1(user);

    game.setBoard(new int[gameData.getBoardGrid()][gameData.getBoardGrid()]);
    game.setUserTurn(1);

    gameService.save(game);

    responseData.success(game, "game successfully created");
    return ResponseEntity.status(HttpStatus.OK).body(responseData);
  }

  @GetMapping("/all")
  public ResponseEntity<Object> iddleGames() {
    ResponseData<Object> responseData = new ResponseData<>();
    try {
      responseData.success(gameService.getAll(), "all game successfully retirieved");
    } catch (NotFoundException e) {
      responseData.failed("error", e.toString());
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
    }
    return ResponseEntity.status(HttpStatus.OK).body(responseData);
  }

  @GetMapping("/iddle")
  public ResponseEntity<Object> findAll() {
    ResponseData<Object> responseData = new ResponseData<>();
    try {
      responseData.success(gameService.getAll(), "all iddle game successfully retirieved");
    } catch (NotFoundException e) {
      responseData.failed("error", e.toString());
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
    }
    return ResponseEntity.status(HttpStatus.OK).body(responseData);
  }

  @GetMapping("/{id}")
  public Game findOne(@PathVariable("id") String id) {
    return gameService.findOne(id ) ;
  }

  @DeleteMapping("/{id}")
  public Game deleteGame(@PathVariable("id") String id) {
    return gameService.removeOne(id);
  }

  @PostMapping("/join/{id}")
  public ResponseEntity<Object> joinGameById(@PathVariable("id") String id) {

    ResponseData<Object> responseData = new ResponseData<>();

    String username = SecurityContextHolder.getContext().getAuthentication().getName();
    User user = userService.getByUsername(username);
    user.setTurns(2);

    Game game;

    try {
      game = gameService.connectToGame(user, id);
    } catch (InvalidParamException | InvalidGameException e) {
      e.printStackTrace();
      responseData.failed("error", "something went wrong");
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e);
    }

    responseData.success(game, "succesfully joined the game");
    return ResponseEntity.status(HttpStatus.OK).body(responseData);
  }

  @PostMapping("/join")
  public ResponseEntity<Object> joinGameByRandom() {

    ResponseData<Object> responseData = new ResponseData<>();

    String username = SecurityContextHolder.getContext().getAuthentication().getName();
    User user = userService.getByUsername(username);
    user.setTurns(2);

    Game game;

    try {
      game = gameService.connectToRandomGame(user);
    } catch (NotFoundException e) {
      e.printStackTrace();
      responseData.failed("error", "something went wrong");
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e);
    }

    responseData.success(game, "succesfully joined the game");
    return ResponseEntity.status(HttpStatus.OK).body(responseData);
  }

  @PostMapping("/play")
  public ResponseEntity<Object> playGame(@RequestBody GamePlay gamePlay) {
    ResponseData<Object> responseData = new ResponseData<>();
    HashMap<String, Object> result;
    try {
      result = gameService.playGame(gamePlay);
      
    } catch (InvalidParamException e) {
      responseData.failed("InvalidParamException", e.getMessage());
      return ResponseEntity.status(HttpStatus.OK).body(responseData);
    } catch (InvalidMoveException e) {
      responseData.failed("InvalidMoveException", e.getMessage());
      return ResponseEntity.status(HttpStatus.OK).body(responseData);
    } catch (InterruptedException e) {
      responseData.failed("InterruptedException", e.getMessage());
      return ResponseEntity.status(HttpStatus.OK).body(responseData);
    } catch (GameDrawException e) {
      responseData.success("game draw", e.getMessage());
      
      Game game = gameService.findOne(gamePlay.getGameId());
      game.setGameStatus(GameStatus.FINISHED);
      gameService.save(game);
      gameService.removeOneFromThread(gamePlay.getGameId());

      responseData.setPayload(game);
      return ResponseEntity.status(HttpStatus.OK).body(responseData);
    }

    if(result.get("winner")!=null){
      Game game = gameService.findOne(gamePlay.getGameId());
      game.setWinner( (User) result.get("winner") );
      game.setGameStatus(GameStatus.FINISHED);
      gameService.save(game);
      gameService.removeOneFromThread(gamePlay.getGameId());

      responseData.success( game, "game draw");
      return ResponseEntity.status(HttpStatus.OK).body(responseData);
    }

    responseData.success( result, "marked successfully");
    return ResponseEntity.status(HttpStatus.OK).body(responseData);
  }

}
