package com.othetoe.game.controller;

import javax.validation.Valid;

import com.othetoe.game.configuration.JwtTokenUtil;
import com.othetoe.game.helper.ResponseData;
import com.othetoe.game.model.dto.JwtRequest;
import com.othetoe.game.model.dto.JwtResponse;
import com.othetoe.game.model.dto.UserData;
import com.othetoe.game.model.entity.User;
import com.othetoe.game.model.service.UserService;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/user")
public class UserController {

  @Autowired
  UserService userService;

  @Autowired
  ModelMapper modelMapper;

  @Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	JwtTokenUtil jwtTokenUtil;

  @GetMapping
  public ResponseEntity<Object> getAllUser() {
    ResponseData<Object> responseData = new ResponseData<>();
    responseData.success(userService.getAll(), "user successfully retrieved");

    return ResponseEntity.status(HttpStatus.OK).body(responseData);
  }

  @PostMapping("/register")
  public ResponseEntity<Object> register(@RequestBody @Valid UserData userData, Errors errors) {
    ResponseData<Object> responseData = new ResponseData<>();
    if (errors.hasErrors()) {
      for (FieldError error : errors.getFieldErrors()) {
        responseData.failed(error.getField(), error.getDefaultMessage());
      }
      responseData.setPayload(errors);
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
    }
    User user = modelMapper.map(userData, User.class);
    User registredUser = null;
    try {
      registredUser = userService.register(user);
    } catch (RuntimeException e) {
      responseData.failed("registration.error", e.getMessage());
      return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
    }
    responseData.success(registredUser, "user successfully registred");
    return ResponseEntity.status(HttpStatus.OK).body(responseData);
  }

  @PostMapping("/login")
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {

		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());

		final UserDetails userDetails = userService
				.loadUserByUsername(authenticationRequest.getUsername());

		final String token = jwtTokenUtil.generateToken(userDetails);

		return ResponseEntity.ok(new JwtResponse(token));
	}

  private void authenticate(String username, String password) throws Exception {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			throw new Exception("INVALID_CREDENTIALS", e);
		}
	}

  @GetMapping("/profile")
  public ResponseEntity<?> getUserProfile(){
    String username = SecurityContextHolder.getContext().getAuthentication().getName();
    User user = userService.getByUsername(username);
    ResponseData<Object> responseData = new ResponseData<>();
    responseData.success(user, "user profie");
    return ResponseEntity.status(HttpStatus.OK).body(responseData);
  }

}
