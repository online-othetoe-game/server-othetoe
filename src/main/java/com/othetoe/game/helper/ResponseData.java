package com.othetoe.game.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ResponseData<T> {
  private boolean success;
  private Map<String, ArrayList<String>> messages = new HashMap<String, ArrayList<String> >();
  private T payload;

  public boolean isSuccess() {
    return success;
  }

  public void success(String message){
    this.success = true;
    ArrayList<String> msg = new ArrayList<String>();
    msg.add(message);
    
    messages.put("success",  msg );
  }

  public void success(T payload, String message){
    this.success = true;
    this.payload = payload;
    ArrayList<String> msg = new ArrayList<String>();
    msg.add(message);
    messages.put("success",  msg );
  }

  public void failed(String key, String message){
    this.success = false;
    if(messages.containsKey(key) ){
      messages.get(key).add(message);
    }
    else{
      ArrayList<String> msg = new ArrayList<String>();
      msg.add(message);
      messages.put(key, msg);
    }
  }

  public void setSuccess(boolean success) {
    this.success = success;
  }

  public T getPayload() {
    return payload;
  }

  public void setPayload(T payload) {
    this.payload = payload;
  }

  public Map<String, ArrayList<String>> getMessages() {
    return messages;
  }
}
