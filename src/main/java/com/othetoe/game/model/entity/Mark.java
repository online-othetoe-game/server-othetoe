package com.othetoe.game.model.entity;


public enum Mark {
    X(1), O(2);

    private Integer value;

    Mark(int value){
        this.value = value;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    
}
