package com.othetoe.game.model.entity;

public enum GameStatus {
  NEW, IN_PROGRESS, FINISHED
}
