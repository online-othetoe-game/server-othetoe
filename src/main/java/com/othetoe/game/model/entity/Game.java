package com.othetoe.game.model.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "games")
public class Game implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid")
  private String id;

  @Column(name = "game_status")
  @Enumerated(EnumType.STRING)
  private GameStatus gameStatus;

  @Column(name = "name")
  private String name;

  @Column(name = "board_grid")
  private int boardGrid;

  @Column(name = "point_to_win")
  private int pointToWin;

  @Column(name = "straight_tiles")
  private int straightTiles;

  @OneToOne
  private User player1;

  @OneToOne
  private User player2;

  @OneToOne
  private User winner;

  @Transient
  private int[][] Board;

  @Transient
  private int userTurn = 1;

  public Game(String id, GameStatus gameState, String name, int boardGrid, int pointToWin, int straightTiles,
      User player1, User player2) {
    this.id = id;
    this.gameStatus = gameState;
    this.name = name;
    this.boardGrid = boardGrid;
    this.pointToWin = pointToWin;
    this.straightTiles = straightTiles;
    this.player1 = player1;
    this.player2 = player2;
  }

  public User getWinner() {
    return winner;
  }



  public void setWinner(User winner) {
    this.winner = winner;
  }

  public int[][] getBoard() {
    return Board;
  }

  public void setBoard(int[][] board) {
    Board = board;
  }

  public int getUserTurn() {
    return userTurn;
  }

  public void setUserTurn(int userTurn) {
    this.userTurn = userTurn;
  }

  public Game() {
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  public User getPlayer1() {
    return player1;
  }

  public void setPlayer1(User player1) {
    this.player1 = player1;
  }

  public User getPlayer2() {
    return player2;
  }

  public void setPlayer2(User player2) {
    this.player2 = player2;
  }

  public GameStatus getGameStatus() {
    return gameStatus;
  }

  public void setGameStatus(GameStatus gameStatus) {
    this.gameStatus = gameStatus;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getBoardGrid() {
    return boardGrid;
  }

  public void setBoardGrid(int boardGrid) {
    this.boardGrid = boardGrid;
  }

  public int getPointToWin() {
    return pointToWin;
  }

  public void setPointToWin(int pointToWin) {
    this.pointToWin = pointToWin;
  }

  public int getStraightTiles() {
    return straightTiles;
  }

  public void setStraightTiles(int straightTiles) {
    this.straightTiles = straightTiles;
  }

}
