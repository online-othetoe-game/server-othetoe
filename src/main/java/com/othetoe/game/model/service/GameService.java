package com.othetoe.game.model.service;

import java.lang.invoke.WrongMethodTypeException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import com.othetoe.game.exception.GameDrawException;
import com.othetoe.game.exception.InvalidGameException;
import com.othetoe.game.exception.InvalidMoveException;
import com.othetoe.game.exception.InvalidParamException;
import com.othetoe.game.exception.NotFoundException;
import com.othetoe.game.model.dto.GamePlay;
import com.othetoe.game.model.entity.Game;
import com.othetoe.game.model.entity.GameStatus;
import com.othetoe.game.model.entity.User;
import com.othetoe.game.model.repository.GameRepo;
import com.othetoe.game.storage.GameStorage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class GameService {
  @Autowired
  GameRepo gameRepo;

  public Game save(Game game) {
    gameRepo.save(game);
    GameStorage.getInstance().setGame(game);
    return game;
  }

  public Game connectToGame(User player2, String gameId) throws InvalidParamException, InvalidGameException {
    if (!GameStorage.getInstance().getGames().containsKey(gameId)) {
      throw new InvalidParamException("Game with provided id doesn't exist");
    }

    Game game = GameStorage.getInstance().getGames().get(gameId);

    if (game.getPlayer2() != null) {
      throw new InvalidGameException("Game is not valid anymore");
    }

    game.setPlayer2(player2);
    game.setGameStatus(GameStatus.IN_PROGRESS);
    gameRepo.save(game);
    GameStorage.getInstance().setGame(game);
    return game;
  }

  public Game connectToRandomGame(User player2) throws NotFoundException {
    Game game = GameStorage.getInstance().getGames().values().stream()
        .filter(it -> it.getGameStatus().equals(GameStatus.NEW))
        .findFirst().orElseThrow(() -> new NotFoundException("Game not found"));
    game.setPlayer2(player2);
    game.setGameStatus(GameStatus.IN_PROGRESS);

    GameStorage.getInstance().setGame(game);
    gameRepo.save(game);
    return game;
  }

  public Map<String, Game> getIddleGame() throws NotFoundException {
    Map<String, Game> games = new HashMap<>();
    for (Game game : GameStorage.getInstance().getGames().values()) {
      if (game.getGameStatus() == GameStatus.NEW) {
        games.put(game.getId(), game);
      }
    }
    return games;
  }

  public Map<String, Game> getAll() throws NotFoundException {
    Map<String, Game> games = GameStorage.getInstance().getGames();
    return games;
  }

  public Game findOne(String id) {
    Optional<Game> game = gameRepo.findById(id);
    if (!game.isPresent()) {
      return null;
    }
    return game.get();
  }

  public Iterable<Game> findAll() {
    return gameRepo.findAll();
  }

  public Game removeOne(String id) {
    Optional<Game> game = gameRepo.findById(id);
    if (game.isPresent()) {
      Game ret = game.get();
      gameRepo.deleteById(id);
      removeOneFromThread(id);
      return ret;
    }
    return null;
  }

  public Game removeOneFromThread(String id) {
    Game game;
    if (GameStorage.getInstance().getGames().containsKey(id)) {
      game = GameStorage.getInstance().getGames().get(id);
      GameStorage.getInstance().getGames().remove(id);
      return game;
    }
    return null;
  }

  public Iterable<Game> findByName(String name) {
    return gameRepo.findByNameContains(name);
  }

  public Iterable<Game> findIddleGame() {
    return gameRepo.findIddleGame();
  }

  // gameplay logic
  public HashMap<String, Object> playGame(GamePlay gamePlay)
      throws InvalidParamException, InvalidMoveException, InterruptedException, GameDrawException {
    if (!GameStorage.getInstance().getGames().containsKey(gamePlay.getGameId())) {
      throw new InvalidParamException("Game with provided id doesn't exist");
    }

    Game game = GameStorage.getInstance().getGames().get(gamePlay.getGameId());
    User player1 = game.getPlayer1();
    User player2 = game.getPlayer2();


    String authorizedUsername = SecurityContextHolder.getContext().getAuthentication().getName();

    User authorizedUser = null;
    if (player1.getUsername().equals(authorizedUsername)) {
      authorizedUser = player1;
    } else if (player2.getUsername().equals(authorizedUsername)) {
      authorizedUser = player2;
    }

    if (authorizedUser == null || authorizedUser.getTurns() != game.getUserTurn() ) {
      throw new InvalidMoveException("wrong turn");
    }

    int coordI = gamePlay.getCoordinateI();
    int coordJ = gamePlay.getCoordinateJ();
    int mark = game.getUserTurn();

    // mark the place
    if (game.getBoard()[coordI][coordJ] == 0) {
      game.getBoard()[coordI][coordJ] = mark;
    } else {
      throw new InvalidMoveException("cell already marked");
    }

    ArrayList<int[]> horizontal = new ArrayList<>();
    ArrayList<int[]> vertical = new ArrayList<>();
    ArrayList<int[]> diagonalLeft = new ArrayList<>();
    ArrayList<int[]> diagonalRight = new ArrayList<>();

    horizontal = isHorizontal(game, coordI, coordJ);
    vertical = isVertical(game, coordI, coordJ);
    diagonalLeft = isDiagonalLeft(game, coordI, coordJ);
    diagonalRight = isDiagonalRight(game, coordI, coordJ);

    boolean okH = horizontal.size() + 1 >= game.getStraightTiles();
    boolean okV = vertical.size() + 1 >= game.getStraightTiles();
    boolean okDR = diagonalRight.size() + 1 >= game.getStraightTiles();
    boolean okDL = diagonalLeft.size() + 1 >= game.getStraightTiles();

    ArrayList<int[]> allResult = new ArrayList<>();

    if (okH || okV || okDR || okDL) {
      if (okH) {
        allResult.addAll(horizontal);
      }
      if (okV) {
        allResult.addAll(vertical);
      }
      if (okDR) {
        allResult.addAll(diagonalRight);
      }
      if (okDL) {
        allResult.addAll(diagonalLeft);
      }
      allResult.add(new int[] { coordI, coordJ });
    }

    for (int[] coords : allResult) {
      game.getBoard()[coords[0]][coords[1]] = 0;
    }

    int totalPoint = allResult.size();

    if (game.getUserTurn() == 1) {
      player1.setPoints(player1.getPoints() + totalPoint);
    } else if (game.getUserTurn() == 2) {
      player2.setPoints(player2.getPoints() + totalPoint);
    }

    if (isDraw(game)) {
      throw new GameDrawException("game draw");
    }

    User winner = null;

    if (player1.getPoints() >= game.getPointToWin()) {
      winner = player1;
    } else if (player2.getPoints() >= game.getPointToWin()) {
      winner = player2;
    }

    HashMap<String, Object> huh = new HashMap<>();
    huh.put("deleted_row", allResult);
    huh.put("winner", winner);

    if (game.getUserTurn() == 1) {
      game.setUserTurn(2);
    } else if (game.getUserTurn() == 2) {
      game.setUserTurn(1);
    }

    return huh;
  }

  // is horizontal straight
  public ArrayList<int[]> isHorizontal(Game game, int posI, int posJ) {
    int boardGrid = game.getBoardGrid();

    ArrayList<int[]> result = new ArrayList<>();

    int[][] board = game.getBoard();
    int core = board[posI][posJ];

    // horizontal ke kiri
    int j = posJ - 1;
    while (j >= 0) {
      if (core == board[posI][j]) {
        result.add(new int[] { posI, j });
      }
      j--;
    }

    // horizontal ke kanan
    j = posJ + 1;
    while (j < boardGrid) {
      if (core == board[posI][j]) {
        result.add(new int[] { posI, j });
      }
      j++;
    }

    return result;

  }

  public ArrayList<int[]> isVertical(Game game, int posI, int posJ) {
    int boardGrid = game.getBoardGrid();
    ArrayList<int[]> result = new ArrayList<>();
    int[][] board = game.getBoard();
    int core = board[posI][posJ];

    // veritcal ke atas
    int i = posI - 1;
    while (i >= 0) {
      if (core == board[i][posJ]) {
        result.add(new int[] { i, posJ });
      }
      i--;
    }

    // veritcal ke bawah
    i = posI + 1;
    while (i < boardGrid) {
      if (core == board[i][posJ]) {
        result.add(new int[] { i, posJ });
      }
      i++;
    }

    return result;

  }

  public ArrayList<int[]> isDiagonalLeft(Game game, int posI, int posJ) {
    int boardGrid = game.getBoardGrid();
    ArrayList<int[]> result = new ArrayList<>();
    int[][] board = game.getBoard();
    int core = board[posI][posJ];

    // diagonal ke kiri atas
    int i = posI - 1;
    int j = posJ - 1;
    while (i >= 0 && j >= 0) {
      if (core == board[i][j]) {
        result.add(new int[] { i, j });
      }
      i--;
      j--;
    }

    // diagonal ke kanan bawah
    i = posI + 1;
    j = posJ + 1;
    while (i < boardGrid && j > boardGrid) {
      if (core == board[i][j]) {
        result.add(new int[] { i, j });
      }
      i++;
      j++;
    }

    return result;

  }

  public ArrayList<int[]> isDiagonalRight(Game game, int posI, int posJ) {
    int boardGrid = game.getBoardGrid();
    ArrayList<int[]> result = new ArrayList<>();
    int[][] board = game.getBoard();
    int core = board[posI][posJ];

    // diagonal ke kanan atas;
    int i = posI - 1;
    int j = posJ + 1;

    while (i >= 0 && j < boardGrid) {
      if (core == board[i][j]) {
        result.add(new int[] { i, j });
      }
      i--;
      j++;
    }

    i = posI + 1;
    j = posJ - 1;

    while (i < boardGrid && j >= 0) {
      if (core == board[i][j]) {
        result.add(new int[] { i, j });
      }
      i++;
      j--;
    }

    return result;

  }

  boolean isDraw(Game game) {
    int boardGrid = game.getBoardGrid();
    int[][] board = game.getBoard();
    for (int i = 0; i < boardGrid; i++) {
      for (int j = 0; j < boardGrid; j++) {
        if (board[i][j] == 0) {
          return false;
        }
      }
    }
    return true;
  }

}
