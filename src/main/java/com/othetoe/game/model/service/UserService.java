package com.othetoe.game.model.service;

import java.util.ArrayList;

import com.othetoe.game.model.entity.User;
import com.othetoe.game.model.repository.UserRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {
  @Autowired
  private UserRepo userRepo;

  @Autowired
  private PasswordEncoder passwordEncoder;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = userRepo.findByUsername(username);
    if (user == null) {
      throw new UsernameNotFoundException("User not found with username: " + username);
    }
    return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
        new ArrayList<>());
  }

  public User register(User user) {
    boolean isUserExist = userRepo.findByUsername(user.getUsername()) != null;
    if (isUserExist) {
      throw new RuntimeException(
          String.format("user with username '%s' already exist ", user.getUsername()));
    }

    String encodedPassword = passwordEncoder.encode(user.getPassword());
    user.setPassword(encodedPassword);
    return userRepo.save(user);
  }

  public Iterable<User> getAll() {
    return userRepo.findAll();
  }

  public User getByUsername(String username){
    return userRepo.findByUsername(username);
  }

}
