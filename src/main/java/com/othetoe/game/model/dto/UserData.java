package com.othetoe.game.model.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import com.othetoe.game.util.validation.FieldsValueMatch;

@FieldsValueMatch.List({
    @FieldsValueMatch(field = "password", fieldMatch = "confirmPassword", message = "Passwords do not match!"),
})

public class UserData {
  @NotEmpty(message = "fullname is required")
  private String fullname;

  @NotEmpty(message = "email is required")
  @Email(message = "invalid email format")
  private String email;

  @NotEmpty(message = "username is required")
  @Pattern(regexp = "^(?=.{6,20}$)(?=\\S+$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$", message = "invalid username format")
  private String username;

  @NotEmpty(message = "password is required")
  @Pattern(regexp = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}", message = "password contains atleast 8 alphanumerical char, and special symbol ")
  private String password;

  @NotEmpty(message = "password is not confirmed")
  private String confirmPassword;

  @NotEmpty(message = "role is required")
  private String role;

  public UserData() {
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getFullname() {
    return fullname;
  }

  public void setFullname(String fullname) {
    this.fullname = fullname;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getConfirmPassword() {
    return confirmPassword;
  }

  public void setConfirmPassword(String confirmPassword) {
    this.confirmPassword = confirmPassword;
  }
}
