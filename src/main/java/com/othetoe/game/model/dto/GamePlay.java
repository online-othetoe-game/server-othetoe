package com.othetoe.game.model.dto;

public class GamePlay {

    private Integer coordinateI;
    private Integer coordinateJ;
    private String gameId;

    public GamePlay(Integer coordinatI, Integer coordinateJ, String gameId) {
        this.coordinateI = coordinatI;
        this.coordinateJ = coordinateJ;
        this.gameId = gameId;
    }

    public Integer getCoordinateI() {
        return coordinateI;
    }

    public void setCoordinateI(Integer coordinateI) {
        this.coordinateI = coordinateI;
    }

    public Integer getCoordinateJ() {
        return coordinateJ;
    }

    public void setCoordinateJ(Integer coordinateJ) {
        this.coordinateJ = coordinateJ;
    }

    public GamePlay() {
    }
    
    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

}
