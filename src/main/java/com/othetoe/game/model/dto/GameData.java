package com.othetoe.game.model.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

public class GameData {
  @NotEmpty(message = "name is required")
  private String name;

  @NotNull(message = "boarGrid is required")
  @Range(min = 1, message = "grid is required")
  private int boardGrid;

  @NotNull(message = "pointToWin is required")
  @Range(min = 1, message = "point is required")
  private int pointToWin;

  @NotNull(message = "straightTiles is required")
  @Range(min = 1, message = "tiles is required")
  private int straightTiles;

  private String player1Id;
  private String gameStatus = "NEW";

  public String getGameStatus() {
    return gameStatus;
  }

  public void setGameStatus(String gameStatus) {
    this.gameStatus = gameStatus;
  }

  public String getPlayer1Id() {
    return player1Id;
  }

  public void setPlayer1Id(String player1Id) {
    this.player1Id = player1Id;
  }

  public GameData() {
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getBoardGrid() {
    return boardGrid;
  }

  public void setBoardGrid(int boardGrid) {
    this.boardGrid = boardGrid;
  }

  public int getPointToWin() {
    return pointToWin;
  }

  public void setPointToWin(int pointToWin) {
    this.pointToWin = pointToWin;
  }

  public int getStraightTiles() {
    return straightTiles;
  }

  public void setStraightTiles(int straightTiles) {
    this.straightTiles = straightTiles;
  }

}
