package com.othetoe.game.model.dto;

import javax.validation.constraints.NotEmpty;

public class CategoryData {
  @NotEmpty(message = "name is required")
  private String name;

  @NotEmpty(message = "email is required")
  private String description;

  public CategoryData(String name, String description) {
    this.name = name;
    this.description = description;
  }

  public CategoryData() {
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
