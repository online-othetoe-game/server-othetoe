package com.othetoe.game.model.repository;

import java.util.List;

import com.othetoe.game.model.entity.User;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends CrudRepository<User, String>{
  public User findByUsername(String username);
  public List<User> findByFullnameContains(String fullname);
}
