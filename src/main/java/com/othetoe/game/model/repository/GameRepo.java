package com.othetoe.game.model.repository;

import java.util.List;
import java.util.Optional;

import com.othetoe.game.model.entity.Game;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRepo extends CrudRepository<Game, String> {
  public List <Game> findByNameContains(String name);
  
  @Query("select g from Game g where g.gameStatus = 1")
  public List <Game> findIddleGame();

  public Optional<Game> findById(String id);
}
